@extends('layouts.app')


@section('content')

    <div class="container" data-ng-controller="CurrencyCtrl">

        <div class="row">
            <div class="col-md-4">
                <button id="btn-get-currency" class="btn btn-primary" ng-click="parseCurrency()">Получить курсы валют</button>
                <div id="listCurrance" class="row">
                    @foreach($arr_currency as $currency)
                        <div class="col-md-12">
                            <a data-name="{{ $currency->name }}" data-id="{{ $currency->id }}" ng-click="updateChart({{ $currency->id }})">{{ $currency->name }}</a>
                        </div>

                    @endforeach

                </div>
            </div>
            <div class="col-md-8">
                <div class="h1" ng-bind="selectCurr"></div>
                <div
                        class="line-chart"
                        line-chart
                        line-post-units="' RUB'"
                        line-data='dataChart'
                        line-xkey='date'
                        line-ykeys='["exchange"]'
                        line-labels='["Exchange"]'
                        line-colors='["#31C0BE"]'>
                </div>
            </div>
        </div>

    </div>


@endsection
