@extends('layouts.app')


@section('content')

    <div class="container">

        <div class="row">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Дата</th>
                    <th>Ответ</th>
                    <th>Время выполнения</th>
                </tr>
                </thead>
                <tbody>
                @foreach($logs as $log)
                    <tr>
                        <td>{{ $log->date }}</td>
                        <td>{{ $log->code_response }}</td>
                        <td>{{ $log->time }}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>

        </div>

    </div>


@endsection
