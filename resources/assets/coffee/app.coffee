baseApp = angular.module('app', ['ui.bootstrap', 'angular.morris-chart'])

baseApp.controller('CurrencyCtrl', ['$scope', '$http', '$location', ($scope, $http, $log, $location, $routeParams) ->

  $scope.dataChart = []
  $scope.selectCurr = ''


  $scope.parseCurrency = () ->
    $("#btn-get-currency").addClass('disable').text('Загрузка...')

    $http({
      method: 'GET',
      url: domain + '/ajax/currency/parse'
    })
      .success((data, status, headers, config) ->
        console.log('parseCurrency')
        $("#btn-get-currency").removeClass('disable').text('Получить курсы валют')

      )


  $scope.updateChart = (currencyId) ->
    $("#listCurrance a").removeClass('active')
    $scope.selectCurr = $('#listCurrance a[data-id="' + currencyId + '"]').addClass('active').data('name')

    $http({
      method: 'POST',
      url: domain + '/ajax/currency/get_exchanges',
      data: {
        'currency_id': currencyId
      }
    })
    .success((data, status, headers, config) ->
      console.log(data)
      $scope.dataChart = data

    )


  $scope.updateChart(1)




] )