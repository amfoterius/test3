<?php

namespace App\Currency;

use Illuminate\Database\Eloquent\Model;
use DB;

class Currency extends Model
{

    public function exchanges()
    {
        return $this->hasMany('App\Currency\Currency_exchange', 'currency_id');
    }

}
