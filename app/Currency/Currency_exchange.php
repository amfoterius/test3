<?php

namespace App\Currency;

use Illuminate\Database\Eloquent\Model;

class Currency_exchange extends Model
{

    public function currency()
    {
        return $this->belongsTo('App\Currency\Currency', 'currency_id');
    }
}
