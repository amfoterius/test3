<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Currency;
use DB;

class HomeController extends Controller
{

    public function index()
    {
        $this->data['arr_currency'] = Currency\Currency::all(['id', 'name']);
        return view('home.index', $this->data);


    }

    public function logs()
    {
        $this->data['logs'] = DB::table('currency_parser_logs')->get();
        return view('home.logs', $this->data);
    }


}
