<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

use App\Currency;
use Mockery\CountValidator\Exception;


class CurrencyController extends Controller
{

    public function get_exchanges(Request $request)
    {
        return Currency\Currency_exchange::select(['date', 'exchange'])->where('currency_id', $request->input('currency_id', 0))->get();
    }


    public function parse(Request $request)
    {
        $start_microtime = microtime();

        $curr_date = new Carbon();

        try
        {
            $sxe = new \SimpleXMLElement(file_get_contents(config('currency.url_parse')));
        }
        catch(Exception $e)
        {
            $time_script = microtime() - $start_microtime;
            DB::table('currency_parser_logs')->insert([
                'date' => new Carbon(),
                'time' => $time_script,
                'code_response' => $http_response_header[0],
                'dump' => json_encode($e),

            ]);

            return abort(500);

        }



        $date_arr = preg_split('/\./', (string)$sxe->attributes()->Date[0]);

        $date_parse = Carbon::createFromDate($date_arr[2], $date_arr[1], $date_arr[0])->toDateString();

        foreach($sxe->Valute as $valute_node)
        {
            $exchange_value = (double)str_replace(',', '.', $valute_node->Value);

            if(($currency = Currency\Currency::where('code', (string)$valute_node->NumCode)->first()) == null)
            {
                $currency = new Currency\Currency();
                $currency->name = (string)$valute_node->Name;
                $currency->code = (string)$valute_node->NumCode;
                $currency->char_code = (string)$valute_node->CharCode;
                $currency->nominal = (int)$valute_node->Nominal;
                $currency->save();


            }

            $currency_exchange = $currency->exchanges()->where('date', $date_parse)->first();

            if($currency_exchange == null)
            {
                $currency_exchange = new Currency\Currency_exchange();
                $currency_exchange->currency_id = $currency->id;
                $currency_exchange->date = $date_parse;
                $currency_exchange->exchange = $exchange_value;

                $currency_exchange->save();

            }
            elseif($currency_exchange->exchange != $exchange_value)
            {
                $currency_exchange->exchange = $exchange_value;

                $currency_exchange->save();

            }





            /*
            $currency->exchanges()->save(new Currency\Currency_exchange([
                'date' => '2016-02-27',
                'exchange' => (double)$valute_node->Value,
            ]));
            */

        }

        $time_script = microtime() - $start_microtime;
        $log = [
            'date' => new Carbon(),
            'time' => $time_script,
            'code_response' => $http_response_header[0],

        ];

        DB::table('currency_parser_logs')->insert($log);
        return $log;





    }


}
