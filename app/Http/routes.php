<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);
Route::get('/logs', ['as' => 'logs', 'uses' => 'HomeController@logs']);

Route::group(['prefix' => 'ajax', 'namespace' => 'Ajax'], function () {

    Route::group(['prefix' => 'currency'], function () {

        Route::post('/', ['uses' => 'CurrencyController@index']);

        Route::get('/parse', ['uses' => 'CurrencyController@parse']);
        Route::post('/get_exchanges', ['uses' => 'CurrencyController@get_exchanges']);
    });






});
