<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CurrencyParserLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_parser_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->double('time', 15, 8);
            $table->string('code_response', 80);
            $table->text('dump')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currency_parser_logs');
    }
}
