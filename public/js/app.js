(function() {
  var baseApp;

  baseApp = angular.module('app', ['ui.bootstrap', 'angular.morris-chart']);

  baseApp.controller('CurrencyCtrl', [
    '$scope', '$http', '$location', function($scope, $http, $log, $location, $routeParams) {
      $scope.dataChart = [];
      $scope.selectCurr = '';
      $scope.parseCurrency = function() {
        $("#btn-get-currency").addClass('disable').text('Загрузка...');
        return $http({
          method: 'GET',
          url: domain + '/ajax/currency/parse'
        }).success(function(data, status, headers, config) {
          console.log('parseCurrency');
          return $("#btn-get-currency").removeClass('disable').text('Получить курсы валют');
        });
      };
      $scope.updateChart = function(currencyId) {
        $("#listCurrance a").removeClass('active');
        $scope.selectCurr = $('#listCurrance a[data-id="' + currencyId + '"]').addClass('active').data('name');
        return $http({
          method: 'POST',
          url: domain + '/ajax/currency/get_exchanges',
          data: {
            'currency_id': currencyId
          }
        }).success(function(data, status, headers, config) {
          console.log(data);
          return $scope.dataChart = data;
        });
      };
      return $scope.updateChart(1);
    }
  ]);

}).call(this);

//# sourceMappingURL=app.js.map
